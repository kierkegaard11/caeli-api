FROM openjdk:11
MAINTAINER kierkegaard11
COPY target/caeli-0.0.1-SNAPSHOT.jar caeli-0.0.1.jar
ENTRYPOINT ["java","-jar","caeli-0.0.1.jar"]
