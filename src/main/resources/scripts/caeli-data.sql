-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: caeli
-- ------------------------------------------------------
-- Server version	5.6.51

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `device`
--

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` VALUES (1,'N','2021-07-27 17:28:51','Caelikrmc2gnz','11:22:33:44:55:66','name 5','location 5',0,0,0,3,'Y'),(2,'Y','2021-07-27 17:31:14','Caelikrmc5j02','11:22:33:44:55:66','name 5','location 5',7,24,62,2,'Y'),(3,'Y','2021-07-27 21:35:08','Caelikrmkv6pv','11:22:33:44:55:66','device 4','location 4',1,28,55,2,'Y'),(4,'Y','2021-07-27 23:12:03','Caelikrmobtpo','11:22:33:44:55:66','device 5','location 5',0,0,0,2,'Y'),(5,'Y','2021-07-28 23:38:38','Caelikro4puna','11:22:33:44:55:66','name 5','location 5',0,0,0,2,'Y'),(6,'N','2021-07-29 14:43:27','Caelikrp11gu9','11:22:33:44:55:66','dnevna soba','Beograd',1,28,55,4,'Y'),(7,'Y','2021-07-29 15:54:02','Caelikrp3k8bb','11:22:33:44:55:66','dnevna soba','Beograd',0,0,0,5,'Y'),(8,'Y','2021-07-29 16:15:04','Caelikrp4ba73','11:22:33:44:55:66','name 5','location 5',1,28,55,5,'Y');
/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `password_reset_token`
--

LOCK TABLES `password_reset_token` WRITE;
/*!40000 ALTER TABLE `password_reset_token` DISABLE KEYS */;
INSERT INTO `password_reset_token` VALUES (2,'b85f9fc0-1659-46db-9f9d-f54c41448ad5',4,'2021-07-30 22:00:00');
/*!40000 ALTER TABLE `password_reset_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'Pera Peric','pera@pera.com','ROLE_USER','Y','2021-07-27 17:10:23','123','$2a$10$MaYRTRh/cCY7N.apZq5CROw8rPlpfhOTGjjioVHhoAR1gRR.4.TBC'),(3,'Zika','zika@zika.com','ROLE_USER','N','2021-07-27 23:13:51',NULL,'$2a$10$MaYRTRh/cCY7N.apZq5CROw8rPlpfhOTGjjioVHhoAR1gRR.4.TBC'),(4,'Maja Miljanic','miljanic.maja0@gmail.com','ROLE_USER','N','2021-07-29 14:35:50',NULL,'$2a$10$jVXGWuuEw81Q4zPs9ylv4e.HjOY1Bx3EPJ1hHdUtGwHJQHtqMijjG'),(5,'Maja Miljanic','miljanic.maja0@gmail.com','ROLE_USER','Y','2021-07-29 15:27:43','123','$2a$10$9OxN4amdbd9q1nLQ0Z5yGOHAKbYnr4zI5FY2bi1ydRyDxPfWCjGrq');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_device_aggr`
--

LOCK TABLES `user_device_aggr` WRITE;
/*!40000 ALTER TABLE `user_device_aggr` DISABLE KEYS */;
INSERT INTO `user_device_aggr` VALUES (1,2,3,'Y','Y','2021-07-27 23:22:05'),(2,3,1,'Y','N','2021-07-27 23:22:05'),(3,2,2,'Y','N','2021-07-27 23:22:05'),(4,2,4,'Y','Y','2021-07-27 23:22:05'),(6,3,3,'N','N','2021-07-28 21:31:53'),(7,2,5,'Y','Y','2021-07-28 23:38:53'),(8,4,6,'Y','N','2021-07-29 14:44:00'),(9,2,6,'N','Y','2021-07-29 14:49:45'),(10,4,5,'N','N','2021-07-29 14:50:47'),(11,5,7,'Y','Y','2021-07-29 15:54:28'),(12,5,5,'N','Y','2021-07-29 16:00:35'),(13,5,8,'Y','Y','2021-07-29 16:15:24'),(14,2,7,'N','Y','2021-09-28 15:03:23');
/*!40000 ALTER TABLE `user_device_aggr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-09 18:37:45
