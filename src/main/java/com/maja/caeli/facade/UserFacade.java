package com.maja.caeli.facade;

import com.maja.caeli.converter.UserConverter;
import com.maja.caeli.converter.UserInfoConverter;
import com.maja.caeli.dto.*;
import com.maja.caeli.exception.InvalidEntityIdException;
import com.maja.caeli.exception.PasswordResetInvalidTokenException;
import com.maja.caeli.model.Device;
import com.maja.caeli.model.PasswordResetToken;
import com.maja.caeli.model.User;
import com.maja.caeli.security.services.TokenService;
import com.maja.caeli.service.DeviceService;
import com.maja.caeli.service.UserService;
import com.maja.caeli.util.JwtResponse;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class UserFacade {

    private static final String TOKEN_INVALID = "Reset password token is invalid!";
    private static final String TOKEN_EXPIRED = "Reset password token expired!";
    private static final String CONFIRM_PASSWORD_WRONG = "Passwords don't match!";
    @Resource
    private UserService userService;
    @Resource
    private UserConverter userConverter;
    @Resource
    private UserInfoConverter userInfoConverter;
    @Resource
    private PasswordEncoder encoder;
    @Resource
    private TokenService tokenService;
    @Resource
    private DeviceService deviceService;
    @Resource
    private UserDeviceAggrService userDeviceAggrService;

    public UserDTO getUserById(long userId) {
        User user = userService.getUserById(userId);
        return (UserDTO) userConverter.convertModelToDto(user);
    }

    public JwtResponseDTO authenticate(LoginDTO loginDTO) {
        JwtResponse jwtResponse = userService.authenticate(loginDTO.getEmail(), loginDTO.getPassword());
        JwtResponseDTO jwtResponseDTO = new JwtResponseDTO();
        User user = jwtResponse.getUser();
        UserDTO userDTO = (UserDTO) userConverter.convertModelToDto(user);
        jwtResponseDTO.setUser(userDTO);
        jwtResponseDTO.setToken(jwtResponse.getToken());
        jwtResponseDTO.setType(jwtResponse.getType());
        return jwtResponseDTO;
    }

    public void saveUser(UserInfoDTO userInfoDTO) {
        User user = (User) userInfoConverter.convertDtoToModel(userInfoDTO);
        user.setPassword(encoder.encode(userInfoDTO.getPassword()));
        userService.registerUser(user);
    }

    @Transactional
    public void updateUserToken(Long userId, String token) {
        userService.updateUserToken(userId, token);
    }

    public void logout(long userId) {
        userService.logout(userId);
    }

    public void sendResetPasswordEmail(String email) {
        User userEntity = userService.getUserByEmail(email);
        String token = tokenService.generateToken();
        userService.createPasswordResetTokenForUser(userEntity, token);
        userService.createAndSendResetTokenEmail(userEntity, token);
    }

    public void validatePasswordResetToken(int userId, String token) {
        PasswordResetToken passwordResetTokenEntity = userService.getPasswordResetTokenByToken(token);
        if (passwordResetTokenEntity.getUser().getId() != userId) {
            throw new PasswordResetInvalidTokenException(TOKEN_INVALID);
        }
        if (passwordResetTokenEntity.getExpiryDate().before(new Date())) {
            throw new PasswordResetInvalidTokenException(TOKEN_EXPIRED);
        }
    }

    public void resetPassword(String token, Long userId, String newPassword, String matchPassword) {
        if (!newPassword.equals(matchPassword)) {
            throw new PasswordResetInvalidTokenException(CONFIRM_PASSWORD_WRONG);
        }
        User user = userService.getPasswordResetTokenByToken(token)
                .getUser();
        if (!user.getId().equals(userId)) {
            throw new InvalidEntityIdException("Provided userId and token are not the pair");
        }
        user.setPassword(encoder.encode(newPassword));
        userService.updateUserInfo(user);
        userService.deletePasswordResetTokenByUserId(user.getId());
    }

    public void deleteUserById(long userId) {
        User user = userService.getUserById(userId);
        List<Device> adminsDevices = deviceService.getDeviceByAdminId(userId);
        for (Device device : adminsDevices) {
            userDeviceAggrService.getUserDeviceAggrsByDeviceId(device.getId()).forEach(userDeviceAggr ->
                    userDeviceAggrService.deleteUserDeviceAggr(userDeviceAggr));
            deviceService.deleteDevice(device.getId());
        }
        userDeviceAggrService.getUserDeviceAggrsByUserId(userId)
                .forEach(userDeviceAggr ->
                        userDeviceAggrService.deleteUserDeviceAggr(userDeviceAggr)
                );
        userService.deleteUser(user);
    }
}
