package com.maja.caeli.facade;

import com.maja.caeli.dao.UserDeviceAggrDAO;
import com.maja.caeli.model.UserDeviceAggr;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserDeviceAggrService {

    @Resource
    private UserDeviceAggrDAO userDeviceAggrDAO;

    public void saveUserDeviceAggr(UserDeviceAggr userDeviceAggr) {
        userDeviceAggrDAO.save(userDeviceAggr);
    }

    public List<UserDeviceAggr> getUserDeviceAggrsByUserId(long userId) {
        return userDeviceAggrDAO.findByUserId(userId);
    }

    public UserDeviceAggr getUserDeviceAggrsByUserIdAndDeviceId(Long userId, long deviceId) {
        return userDeviceAggrDAO.findByUserIdAndDeviceId(userId, deviceId);
    }

    public void deleteUserDeviceAggr(long deviceId, Long userId) {
        UserDeviceAggr userDeviceAggr = userDeviceAggrDAO.findByUserIdAndDeviceId(userId, deviceId);
        if (userDeviceAggr != null) {
            userDeviceAggr.setActive(false);
            userDeviceAggrDAO.save(userDeviceAggr);
        }
    }

    public void deleteUserDeviceAggr(UserDeviceAggr userDeviceAggr) {
        userDeviceAggr.setActive(false);
        userDeviceAggrDAO.save(userDeviceAggr);
    }

    public List<UserDeviceAggr> getUserDeviceAggrsByDeviceId(long deviceId) {
        return userDeviceAggrDAO.findByDeviceId(deviceId);
    }
}
