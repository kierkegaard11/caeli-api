package com.maja.caeli.facade.validator;

import com.maja.caeli.exception.DeviceOwnerException;
import com.maja.caeli.model.Device;
import com.maja.caeli.model.User;
import org.springframework.stereotype.Component;

@Component
public class AdminValidator {

    private static final String USER_NOT_OWNER_OF_DEVICE = "User with id=% is not owner of this device!";

    public void validate(User user, Device device) {
        if (device.getAdmin().getId() != user.getId()) {
            throw new DeviceOwnerException(
                    String.format(USER_NOT_OWNER_OF_DEVICE, user.getId())
            );
        }
    }
}
