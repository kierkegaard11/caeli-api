package com.maja.caeli.facade;

import com.maja.caeli.converter.DeviceConverter;
import com.maja.caeli.converter.UserConverter;
import com.maja.caeli.dto.DeviceDTO;
import com.maja.caeli.dto.DeviceUpdateDTO;
import com.maja.caeli.dto.DeviceShareCodeDTO;
import com.maja.caeli.dto.UserDTO;
import com.maja.caeli.exception.InvalidEntityIdException;
import com.maja.caeli.exception.InvalidLicenceException;
import com.maja.caeli.facade.validator.AdminValidator;
import com.maja.caeli.model.Device;
import com.maja.caeli.model.User;
import com.maja.caeli.model.UserDeviceAggr;
import com.maja.caeli.service.DeviceService;
import com.maja.caeli.service.UserService;
import com.maja.caeli.util.LicenceUtil;
import com.maja.caeli.util.Util;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class DeviceFacade {

    private static final String LICENCE_ALREADY_USED = "Licence %s is already used";
    private static final String DEVICE_NOT_INITIALIZED = "Device is not initialized!";
    private static final String SHARE_CODE_SUCCESS = "Success!";

    @Resource
    private DeviceConverter deviceConverter;
    @Resource
    private DeviceService deviceService;
    @Resource
    private UserService userService;
    @Resource
    private UserDeviceAggrService userDeviceAggrService;
    @Resource
    private AdminValidator adminValidator;
    @Resource
    private UserConverter userConverter;

    public DeviceDTO createNewDevice(DeviceDTO deviceDTO) {
        Device device = deviceService.getDeviceByLicence(deviceDTO.getLicence());
        if (device.getInitialized()) {
            throw new InvalidLicenceException(
                    String.format(LICENCE_ALREADY_USED, deviceDTO.getLicence())
            );
        }
        device.setName(deviceDTO.getName());
        device.setLocation(deviceDTO.getLocation());
        User user = userService.getUserById(deviceDTO.getAdminId());
        device.setAdmin(user);
        device.setInitialized(true);
        Device savedDevice = deviceService.saveDevice(device);
        connectDeviceAndUserAdmin(savedDevice, user);
        return (DeviceDTO) deviceConverter.convertModelToDto(savedDevice);
    }

    private void connectDeviceAndUserAdmin(Device savedDevice, User user) {
        UserDeviceAggr userDeviceAggr = new UserDeviceAggr();
        userDeviceAggr.setDevice(savedDevice);
        userDeviceAggr.setUser(user);
        userDeviceAggr.setAdmin(true);
        userDeviceAggrService.saveUserDeviceAggr(userDeviceAggr);
    }

    public DeviceDTO createDefaultDevice(String macAddress) {
        Device device = new Device();
        device.setLicence(LicenceUtil.generateDeviceLicence());
        device.setMacAddress(macAddress);
        deviceService.saveDevice(device);
        return (DeviceDTO) deviceConverter.convertModelToDto(device);
    }

    public DeviceDTO updateDevice(long deviceId, DeviceUpdateDTO deviceDTO, User user) {
        Device device = deviceService.getDeviceById(deviceId);
        if (!device.getInitialized()) {
            throw new InvalidEntityIdException(DEVICE_NOT_INITIALIZED);
        }
        adminValidator.validate(user, device);
        device.setName(deviceDTO.getName());
        device.setLocation(deviceDTO.getLocation());
        Device updatedDevice = deviceService.saveDevice(device);
        return (DeviceDTO) deviceConverter.convertModelToDto(updatedDevice);
    }

    public void deleteDevice(long deviceId, User user) {
        Device device = deviceService.getDeviceById(deviceId);
        if (!device.getInitialized()) {
            throw new InvalidEntityIdException(DEVICE_NOT_INITIALIZED);
        }
        if (device.getAdmin().getId() == user.getId()) {
            userDeviceAggrService.getUserDeviceAggrsByDeviceId(deviceId)
                    .forEach(userDeviceAggr ->
                            userDeviceAggrService.deleteUserDeviceAggr(userDeviceAggr)
                    );
            deviceService.deleteDevice(deviceId);
        }
        userDeviceAggrService.deleteUserDeviceAggr(deviceId, user.getId());
    }

    public DeviceDTO getDeviceById(long deviceId) {
        return (DeviceDTO) deviceConverter
                .convertModelToDto(deviceService.getDeviceById(deviceId));
    }

    public void updateDeviceStates(String licence, int airQuality, int humidity, int temperature) {
        Device device = deviceService.getDeviceByLicence(licence);
        device.setHumidity(humidity);
        device.setTemperature(temperature);
        device.setAirQuality(Util.getAirQuality(airQuality));
        device.setPpm(airQuality);
        deviceService.saveDevice(device);
    }

    public List<DeviceDTO> getDevicesByUserId(long userId) {
        List<UserDeviceAggr> userDeviceAggrs = userDeviceAggrService.getUserDeviceAggrsByUserId(userId);
        List<DeviceDTO> deviceDTOs = new ArrayList<>();
        for (UserDeviceAggr userDeviceAggr : userDeviceAggrs) {
            Device device = deviceService.getDeviceById(userDeviceAggr.getDevice().getId());
            deviceDTOs.add((DeviceDTO) deviceConverter.convertModelToDto(device));
        }
        return deviceDTOs;
    }

    public boolean deviceBelongsToUser(Long userId, long deviceId) {
        return userDeviceAggrService.getUserDeviceAggrsByUserIdAndDeviceId(userId, deviceId) != null;
    }

    public DeviceShareCodeDTO getShareCodeByDeviceId(long deviceId, User user) {
        Device device = deviceService.getDeviceById(deviceId);
        if (!device.getInitialized()) {
            throw new InvalidEntityIdException(DEVICE_NOT_INITIALIZED);
        }
        adminValidator.validate(user, device);
        DeviceShareCodeDTO deviceShareCodeDTO = new DeviceShareCodeDTO();
        deviceShareCodeDTO.setShareCode(LicenceUtil.generateDeviceShareCode(device.getLicence()));
        deviceShareCodeDTO.setSuccess(true);
        deviceShareCodeDTO.setMessage(SHARE_CODE_SUCCESS);
        return deviceShareCodeDTO;
    }

    public DeviceDTO shareDeviceWithUser(Long userId, String licenceCode) {
        UserDeviceAggr userDeviceAggr = new UserDeviceAggr();
        User user = userService.getUserById(userId);
        Device device = deviceService.getDeviceByLicence(licenceCode);
        if (!device.getInitialized()) {
            throw new InvalidEntityIdException(DEVICE_NOT_INITIALIZED);
        }
        if (userDeviceAggrService.getUserDeviceAggrsByUserIdAndDeviceId(userId, device.getId()) == null) {
            userDeviceAggr.setUser(user);
            userDeviceAggr.setDevice(device);
            userDeviceAggrService.saveUserDeviceAggr(userDeviceAggr);
        }
        return (DeviceDTO) deviceConverter.convertModelToDto(device);
    }

    public List<UserDTO> getDeviceMembers(long deviceId) {
        List<UserDeviceAggr> userDeviceAggrs = userDeviceAggrService.getUserDeviceAggrsByDeviceId(deviceId);
        List<UserDTO> userDTOS = new ArrayList<>();
        userDeviceAggrs.forEach(userDeviceAggr -> {
            User user = userService.getUserById(userDeviceAggr.getUser().getId());
            userDTOS.add((UserDTO) userConverter.convertModelToDto(user));
        });
        return userDTOS;
    }
}
