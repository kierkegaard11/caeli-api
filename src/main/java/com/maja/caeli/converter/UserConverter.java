package com.maja.caeli.converter;

import com.maja.caeli.dto.BaseDTO;
import com.maja.caeli.dto.UserDTO;
import com.maja.caeli.model.BaseModel;
import com.maja.caeli.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter extends GenericConverter {

    @Override
    protected void convertModelToDto(BaseModel source, BaseDTO target) {
        User user = (User) source;
        UserDTO userDTO = (UserDTO) target;
        userDTO.setUserId(user.getId());
        userDTO.setFullName(user.getFullName());
        userDTO.setEmail(user.getEmail());
    }

    @Override
    protected BaseDTO createBaseDto() {
        return new UserDTO();
    }

    @Override
    protected void convertDtoToModel(BaseDTO source, BaseModel target) {
        UserDTO userDTO = (UserDTO) source;
        User user = (User) target;
        user.setFullName(userDTO.getFullName());
        user.setEmail(userDTO.getEmail());
        user.setRole("ROLE_USER");
    }

    @Override
    protected BaseModel createBaseModel() {
        return new User();
    }
}
