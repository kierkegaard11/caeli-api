package com.maja.caeli.converter;

import com.maja.caeli.dto.BaseDTO;
import com.maja.caeli.dto.UserDTO;
import com.maja.caeli.dto.UserInfoDTO;
import com.maja.caeli.model.BaseModel;
import com.maja.caeli.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserInfoConverter extends GenericConverter {

    private static final String ROLE_USER = "ROLE_USER";

    @Override
    protected void convertModelToDto(BaseModel source, BaseDTO target) {
        User user = (User) source;
        UserInfoDTO userDTO = (UserInfoDTO) target;
        userDTO.setUserId(user.getId());
        userDTO.setFullName(user.getFullName());
        userDTO.setEmail(user.getEmail());
    }

    @Override
    protected BaseDTO createBaseDto() {
        return new UserInfoDTO();
    }

    @Override
    protected void convertDtoToModel(BaseDTO source, BaseModel target) {
        UserInfoDTO userDTO = (UserInfoDTO) source;
        User user = (User) target;
        user.setFullName(userDTO.getFullName());
        user.setEmail(userDTO.getEmail());
        user.setRole(ROLE_USER);
    }

    @Override
    protected BaseModel createBaseModel() {
        return new User();
    }
}
