package com.maja.caeli.converter;

import com.maja.caeli.dto.BaseDTO;
import com.maja.caeli.dto.DeviceDTO;
import com.maja.caeli.model.BaseModel;
import com.maja.caeli.model.Device;
import com.maja.caeli.model.User;
import org.springframework.stereotype.Component;

@Component
public class DeviceConverter extends GenericConverter {
    @Override
    protected void convertModelToDto(BaseModel source, BaseDTO target) {
        Device device = (Device) source;
        DeviceDTO deviceDTO = (DeviceDTO) target;
        deviceDTO.setId(device.getId());
        deviceDTO.setLicence(device.getLicence());
        deviceDTO.setMacAddress(device.getMacAddress());
        deviceDTO.setName(device.getName());
        deviceDTO.setLocation(device.getLocation());
        deviceDTO.setAirQuality(device.getAirQuality());
        deviceDTO.setHumidity(device.getHumidity());
        deviceDTO.setTemperature(device.getTemperature());
        deviceDTO.setPpm(device.getPpm());
        deviceDTO.setCreated(device.getCreated());
        if (device.getInitialized()) {
            deviceDTO.setAdminId(device.getAdmin().getId());
        }
    }

    @Override
    protected BaseDTO createBaseDto() {
        return new DeviceDTO();
    }

    @Override
    protected void convertDtoToModel(BaseDTO source, BaseModel target) {
        DeviceDTO deviceDTO = (DeviceDTO) source;
        Device device = (Device) target;
        device.setName(deviceDTO.getName());
        device.setLocation(deviceDTO.getLocation());
        device.setTemperature(deviceDTO.getTemperature());
        device.setHumidity(deviceDTO.getHumidity());
        device.setPpm(device.getPpm());
        User user = new User();
        user.setId(deviceDTO.getAdminId());
        device.setAdmin(user);
    }

    @Override
    protected BaseModel createBaseModel() {
        return new Device();
    }
}
