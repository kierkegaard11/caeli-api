package com.maja.caeli.converter;

import com.maja.caeli.dto.BaseDTO;
import com.maja.caeli.model.BaseModel;

public abstract class GenericConverter {

    public BaseDTO convertModelToDto(BaseModel source) {
        BaseDTO target = createBaseDto();
        convertModelToDto(source, target);
        return target;
    }

    public BaseModel convertDtoToModel(BaseDTO source) {
        BaseModel target = createBaseModel();
        convertDtoToModel(source, target);
        return target;
    }

    protected abstract void convertModelToDto(BaseModel source, BaseDTO target);

    protected abstract BaseDTO createBaseDto();

    protected abstract void convertDtoToModel(BaseDTO source, BaseModel target);

    protected abstract BaseModel createBaseModel();
}
