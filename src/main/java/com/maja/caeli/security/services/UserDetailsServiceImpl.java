package com.maja.caeli.security.services;

import com.maja.caeli.exception.InvalidEmailException;
import com.maja.caeli.model.User;
import com.maja.caeli.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        User user = Optional.ofNullable(userService.getUserByEmail(email))
                .orElseThrow(
                        () -> new InvalidEmailException("User Not Found with -> email : " + email));

        return UserPrinciple.build(user);
    }
}
