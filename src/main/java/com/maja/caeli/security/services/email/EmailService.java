package com.maja.caeli.security.services.email;

import com.maja.caeli.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    @Value("${email.template.reset.password}")
    private String resetPasswordEmailTemplate;

    @Value("${email.subject.reset.password}")
    private String resetPasswordEmailSubject;

    @Value("${spring.mail.username}")
    private String springMailUsername;

    @Qualifier(value = "JavaMailConfig")
    @Autowired
    private JavaMailSender mailSender;

    public void sendResetPasswordEmail(User user, String resetPasswordLink) {
        try {
            SimpleMailMessage email = new SimpleMailMessage();
            String body = String.format(resetPasswordEmailTemplate, user.getFullName(), resetPasswordLink);
            email.setSubject(resetPasswordEmailSubject);
            email.setText(body);
            email.setTo(user.getEmail());
            email.setFrom(springMailUsername);
            mailSender.send(email);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
