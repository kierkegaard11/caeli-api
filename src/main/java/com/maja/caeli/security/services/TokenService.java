package com.maja.caeli.security.services;

import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.UUID;

@Service
public class TokenService {

    public String generateToken() {
        return UUID.randomUUID().toString();
    }

    public String generateCode() {
        Random random = new Random();
        int number = random.nextInt(999999);
        return String.format("%06d", number);
    }

}
