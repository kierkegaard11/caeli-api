package com.maja.caeli.util;

import com.maja.caeli.model.User;

public class JwtResponse {

    private String token;
    private String type;
    private User user;

    public JwtResponse(String token, User user) {
        this.token = token;
        this.type = "Bearer";
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
