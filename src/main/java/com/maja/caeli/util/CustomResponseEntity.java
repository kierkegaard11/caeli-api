package com.maja.caeli.util;

import com.maja.caeli.exception.CaeliException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class CustomResponseEntity {

    private CustomResponseEntity() {
    }

    public static ResponseEntity<?> success(Object data) {
        return ResponseEntity.ok(new CustomResponse(data, 10000));
    }

    public static ResponseEntity<?> success(Page page) {
        Map<String, Object> response = new HashMap<>();
        response.put("items", page.getContent());
        response.put("currentPage", page.getNumber());
        response.put("totalItems", page.getTotalElements());
        response.put("totalPages", page.getTotalPages());
        return ResponseEntity.ok(new CustomResponse(response, 10000));
    }

    public static ResponseEntity<?> ok(Object body) {
        return ResponseEntity.ok(body);
    }

    public static ResponseEntity<?> ok(String message) {
        return ResponseEntity.ok(new CustomResponse(message));
    }

    public static ResponseEntity<?> error(String message) {
        return new ResponseEntity<>(new CustomResponse(message), HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<?> error(String message, int status) {
        return new ResponseEntity<>(new CustomResponse(message, status), HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<?> error(Map<String, String> map) {
        return new ResponseEntity<>(new CustomResponse(map, CaeliError.INPUT_VALIDATION_ERROR.getCode()), HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<?> error(String message, String stack) {
        return new ResponseEntity<>(new CustomResponse(message, stack), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity<?> error(String message, int status, String stack) {
        return new ResponseEntity<>(new CustomResponse(message, status, stack), HttpStatus.CONFLICT);
    }

    public static ResponseEntity<?> error(CaeliException cex) {
        return new ResponseEntity<>(new CustomResponse(cex.getMessage(), cex.getStatus(), Util.getStackTraceString(cex)), HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<?> error(RuntimeException dcbe) {
        return new ResponseEntity<>(new CustomResponse(dcbe.getMessage(), 10001, Util.getStackTraceString(dcbe)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity<?> error(Exception dcbe, HttpStatus httpStatus) {
        return new ResponseEntity<>(new CustomResponse(dcbe.getMessage(), 10001, Util.getStackTraceString(dcbe)), httpStatus);
    }

    public static ResponseEntity<?> error(Exception dcbe, int httpStatus) {
        return ResponseEntity.status(httpStatus).body(dcbe.getMessage());
    }
}
