package com.maja.caeli.util;

public enum CaeliError {
    INPUT_VALIDATION_ERROR(10010, "INFO"),
    INVALID_ID(10020, "ERROR"),
    INVALID_EMAIL(10030, "ERROR"),
    INVALID_TOKEN(10040, "ERROR"),
    INVALID_LICENCE(10050, "ERROR"),
    OWNER_RESTRICTION(10060, "ERROR"),
    NOT_LOGGED_IN(10070, "ERROR");

    private int code;
    private String level;

    CaeliError(int code, String level) {
        this.code = code;
        this.level = level;
    }

    public int getCode() {
        return this.code;
    }

    public String getLevel() {
        return this.level;
    }

    public boolean isError() {
        return this.level.equalsIgnoreCase("ERROR");
    }
}
