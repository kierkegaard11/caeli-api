package com.maja.caeli.util;

public class LicenceUtil {

    private static final String DEVICE_LICENCE_PREFIX = "Caeli";
    public static final String SHARE_CODE_PREFIX = "I";
    public static final String SHARE_CODE_DELIMITER = "-";

    public static String generateDeviceLicence() {
        return DEVICE_LICENCE_PREFIX + generateUniqueToken();
    }

    private static String generateTimeToken() {
        long licenceTime = System.currentTimeMillis();
        return Long.toString(licenceTime, 36);
    }

    private static String generateUniqueToken() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException ignored) {

        }
        return generateTimeToken();
    }

    public static String generateDeviceShareCode(String licenceCode) {
        String shareCode = SHARE_CODE_PREFIX + generateTimeToken() + SHARE_CODE_DELIMITER + licenceCode;
        return shareCode;
    }

    public static String getLicenceCodeFromShareCode(String shareCode) {
        String[] shareCodeParts = shareCode.split(SHARE_CODE_DELIMITER);
        return shareCodeParts[1];
    }

}
