package com.maja.caeli.util;

import com.maja.caeli.model.state.AirQuality;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Util {

    public static String getStackTraceString(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    public static AirQuality getAirQuality(int airQuality) {
        if (airQuality == 0) {
            return AirQuality.INIT;
        }
        if (airQuality > 0 && airQuality <= 350) {
            return AirQuality.EXCELLENT;
        }
        if (airQuality > 350 && airQuality <= 450) {
            return AirQuality.GOOD;
        }
        if (airQuality > 450 && airQuality <= 1000) {
            return AirQuality.UNHEALTHY;
        }
        if (airQuality > 1000) {
            return AirQuality.HAZARDOUS;
        }
        return AirQuality.ERROR;
    }
}
