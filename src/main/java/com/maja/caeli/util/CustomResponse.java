package com.maja.caeli.util;

public class CustomResponse {

    private String message;
    private Object data;
    private int status;
    private String stack;

    public CustomResponse(String message, int status) {
        this.message = message;
        this.status = status;
    }

    public CustomResponse(String message, int status, String stack) {
        this.message = message;
        this.status = status;
        this.stack = stack;
    }

    public CustomResponse(String message, String stack) {
        this.message = message;
        this.stack = stack;
    }

    public CustomResponse(String message) {
        this.message = message;
    }

    public CustomResponse(Object data, int status) {
        this.data = data;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStack() {
        return stack;
    }

    public void setStack(String stack) {
        this.stack = stack;
    }
}
