package com.maja.caeli.service;

import com.maja.caeli.dao.DeviceDAO;
import com.maja.caeli.exception.InvalidEntityIdException;
import com.maja.caeli.exception.InvalidLicenceException;
import com.maja.caeli.model.Device;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DeviceService {

    private static final String DEVICE_NOT_FOUND_BY_LICENCE = "Device with licence = %s does not exist";
    private static final String DEVICE_NOT_FOUND_BY_ID = "Device with id = %s does not exist";
    private static final String DEVICE_NOT_FOUND_BY_ADMIN_ID = "Device not found by admin id.";

    @Resource
    private DeviceDAO deviceDAO;

    public Device saveDevice(Device device) {
        return deviceDAO.save(device);
    }

    public Device getDeviceByLicence(String licence) {
        return deviceDAO.findByLicence(licence).orElseThrow(() ->
                new InvalidLicenceException(
                        String.format(DEVICE_NOT_FOUND_BY_LICENCE, licence)
                ));
    }

    public Device getDeviceById(long deviceId) {
        return deviceDAO.findById(deviceId).orElseThrow(() ->
                new InvalidEntityIdException(
                        String.format(DEVICE_NOT_FOUND_BY_ID, deviceId)
                ));
    }

    public List<Device> getDeviceByAdminId(long adminId) {
        return deviceDAO.findByAdminId(adminId);
    }

    public void deleteDevice(long deviceId) {
        if (!deviceDAO.findById(deviceId).isPresent()) {
            throw new InvalidEntityIdException(
                    String.format(DEVICE_NOT_FOUND_BY_ID, deviceId)
            );
        } else {
            Device device = deviceDAO.findById(deviceId).get();
            device.setActive(false);
            deviceDAO.save(device);
        }

    }
}
