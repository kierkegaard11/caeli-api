package com.maja.caeli.service;

import com.maja.caeli.api.Controller;
import com.maja.caeli.dao.PasswordResetTokenDAO;
import com.maja.caeli.dao.UserDAO;
import com.maja.caeli.exception.InvalidEmailException;
import com.maja.caeli.exception.InvalidEntityIdException;
import com.maja.caeli.exception.PasswordResetInvalidTokenException;
import com.maja.caeli.model.PasswordResetToken;
import com.maja.caeli.model.User;
import com.maja.caeli.security.jwt.JwtProvider;
import com.maja.caeli.security.services.UserPrinciple;
import com.maja.caeli.security.services.email.EmailService;
import com.maja.caeli.util.DateConverter;
import com.maja.caeli.util.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

@Service
public class UserService {

    private static final String USER_NOT_FOUND_BY_ID = "User with id=%s does not exist";
    private static final String USER_NOT_FOUND_BY_EMAIL = "User with email %s does not exist!";
    private static final String USER_WITH_EMAIL_EXISTS = "User with email %s already exists!";
    private static final String RESET_PASSWORD_LINK_PATTERN = "%s%s?userId=%s&token=%s";

    @Value("${application.base.url}")
    protected String applicationBaseUrl;

    @Resource
    private AuthenticationManager authenticationManager;
    @Resource
    private JwtProvider jwtProvider;
    @Resource
    private UserDAO userDAO;
    @Resource
    private PasswordResetTokenDAO passwordResetTokenDAO;
    @Resource
    private EmailService emailService;

    public User getUserById(long id) {
        return userDAO.findById(id).orElseThrow(() ->
                new InvalidEntityIdException(
                        String.format(USER_NOT_FOUND_BY_ID, id)
                ));
    }

    public User getUserByEmail(String email) {
        return userDAO.findByEmail(email)
                .orElseThrow(() -> new InvalidEmailException(
                        String.format(USER_NOT_FOUND_BY_EMAIL, email)
                ));
    }

    public JwtResponse authenticate(String email, String password) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, password));

        SecurityContextHolder.getContext()
                .setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserPrinciple userPrinciple = (UserPrinciple) authentication.getPrincipal();
        return new JwtResponse(jwt, userPrinciple.getUser());
    }

    public void registerUser(User user) {
        if (userDAO.findByEmail(user.getEmail()).isPresent()) {
            throw new InvalidEmailException(
                    String.format(USER_WITH_EMAIL_EXISTS, user.getEmail())
            );
        }
        userDAO.save(user);
    }

    public void updateUserToken(Long userId, String token) {
        userDAO.updateUserToken(userId, token);
    }

    public void logout(long userId) {
        if (!userDAO.findById(userId).isPresent()) {
            throw new InvalidEntityIdException(
                    String.format(USER_NOT_FOUND_BY_ID, userId));
        } else {
            userDAO.deleteToken(userId);
        }
    }

    public void createPasswordResetTokenForUser(User user, String token) {
        Optional<PasswordResetToken> passwordResetToken = passwordResetTokenDAO.findByUserId(user.getId());
        if (passwordResetToken.isPresent()) {
            updatePasswordResetToken(passwordResetToken.get(), token);
        } else {
            savePasswordResetToken(user, token);
        }
    }

    private void savePasswordResetToken(User user, String token) {
        PasswordResetToken passwordResetToken = new PasswordResetToken();
        passwordResetToken.setUser(user);
        passwordResetToken.setToken(token);
        passwordResetToken.setExpiryDate(getExpiryDate());
        passwordResetTokenDAO.save(passwordResetToken);
    }

    private void updatePasswordResetToken(PasswordResetToken passwordResetToken, String token) {
        passwordResetToken.setToken(token);
        passwordResetToken.setExpiryDate(getExpiryDate());
        passwordResetTokenDAO.update(
                passwordResetToken.getId(),
                passwordResetToken.getToken(),
                passwordResetToken.getExpiryDate()
        );
    }

    private Date getExpiryDate() {
        return DateConverter.getDateFromLocalDate(LocalDate.now().plusDays(getResetPasswordTokenExpirationDays()));
    }

    private int getResetPasswordTokenExpirationDays() {
        return 2;
    }

    public void createAndSendResetTokenEmail(User user, String token) {
        emailService.sendResetPasswordEmail(user, getResetPasswordLink(user.getId(), token));
    }

    private String getResetPasswordLink(Long userId, String token) {
        return String.format(
                RESET_PASSWORD_LINK_PATTERN,
                applicationBaseUrl,
                Controller.RESET_PASSWORD_PAGE_URL,
                userId,
                token
        );
    }

    public PasswordResetToken getPasswordResetTokenByToken(String token) {
        return passwordResetTokenDAO.findByToken(token)
                .orElseThrow(() -> new PasswordResetInvalidTokenException("Invalid token"));
    }

    public void updateUserInfo(User user) {
        if (!userDAO.findById(user.getId()).isPresent()) {
            throw new InvalidEntityIdException(
                    String.format(USER_NOT_FOUND_BY_ID,
                            user.getId()));
        }
        userDAO.save(user);
    }

    public void deletePasswordResetTokenByUserId(Long userId) {
        if (passwordResetTokenDAO.findByUserId(userId).isPresent()) {
            passwordResetTokenDAO.deleteByUserId(userId);
        }
    }

    public void deleteUser(User user) {
        if (!userDAO.findById(user.getId()).isPresent()) {
            throw new InvalidEntityIdException(
                    String.format(USER_NOT_FOUND_BY_ID,
                            user.getId()));
        }
        user.setActive(false);
        userDAO.save(user);
    }
}
