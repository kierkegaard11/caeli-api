package com.maja.caeli.exception;

import com.maja.caeli.util.CaeliError;

public class InvalidEmailException extends CaeliException {

    public InvalidEmailException(String message) {
        super(message, CaeliError.INVALID_EMAIL.getCode(), "InvalidEmailException", CaeliError.INVALID_EMAIL.getLevel());
    }
}
