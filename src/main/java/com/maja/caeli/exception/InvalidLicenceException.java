package com.maja.caeli.exception;

import com.maja.caeli.util.CaeliError;

public class InvalidLicenceException extends CaeliException {

    public InvalidLicenceException(String message) {
        super(message, CaeliError.INVALID_LICENCE.getCode(), "InvalidLicenceException", CaeliError.INVALID_LICENCE.getLevel());
    }
}
