package com.maja.caeli.exception;

import com.maja.caeli.util.CaeliError;

public class PasswordResetInvalidTokenException extends CaeliException {
    public PasswordResetInvalidTokenException(String message) {
        super(message, CaeliError.INVALID_TOKEN.getCode(), "PasswordResetInvalidTokenException", CaeliError.INVALID_TOKEN.getLevel());
    }
}
