package com.maja.caeli.exception;

import com.maja.caeli.util.CaeliError;

public class UserNotLoggedInException extends CaeliException {

    public UserNotLoggedInException(String message) {
        super(message, CaeliError.NOT_LOGGED_IN.getCode(), "UserNotLoggedInException", CaeliError.NOT_LOGGED_IN.getLevel());
    }
}

