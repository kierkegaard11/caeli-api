package com.maja.caeli.exception;

import com.maja.caeli.util.CaeliError;

public class DeviceOwnerException extends CaeliException {

    public DeviceOwnerException(String message) {
        super(message, CaeliError.OWNER_RESTRICTION.getCode(), "DeviceOwnerException", CaeliError.OWNER_RESTRICTION.getLevel());
    }
}
