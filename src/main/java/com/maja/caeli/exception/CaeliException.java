package com.maja.caeli.exception;

public class CaeliException extends RuntimeException {

    private int status;
    private String code;
    private String message;
    private String severity;

    public CaeliException(String message, int status, String code, String severity) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.severity = severity;
    }

    public int getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public String getSeverity() {
        return severity;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
