package com.maja.caeli.exception;

import com.maja.caeli.util.CaeliError;

public class InvalidEntityIdException extends CaeliException {

    public InvalidEntityIdException(String message) {
        super(message, CaeliError.INVALID_ID.getCode(), "InvalidEntityIdException", CaeliError.INVALID_ID.getLevel());
    }
}
