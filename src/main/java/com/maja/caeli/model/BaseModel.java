package com.maja.caeli.model;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Objects;

@MappedSuperclass
@Where(clause = "active = 'Y'")
public class BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    @Type(type = "org.hibernate.type.YesNoType")
    private Boolean active = Boolean.TRUE;

    @Column(nullable = false)
    private ZonedDateTime created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    @PrePersist
    @PreUpdate
    public void setDates() {
        ZonedDateTime now = ZonedDateTime.now();
        if (getCreated() == null) {
            setCreated(now);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        BaseModel m = (BaseModel) obj;
        if (m == null || m.getId() == null || getId() == null) {
            return false;
        }
        return getId().longValue() == m.getId().longValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
