package com.maja.caeli.model;

import com.maja.caeli.model.state.AirQuality;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "device")
@Where(clause = "active = 'Y'")
public class Device extends BaseModel {

    @Column(nullable = false)
    private String licence;

    @Column(name = "mac_address", nullable = false)
    private String macAddress;

    @Column
    private String name;

    @Column
    private String location;

    @Column(name = "air_quality")
    @Enumerated(EnumType.ORDINAL)
    private AirQuality airQuality = AirQuality.INIT;

    @Column
    private Integer temperature = 0;

    @Column
    private Integer humidity = 0;

    @Column
    private Integer ppm = 0;

    @ManyToOne
    @JoinColumn(name = "admin_id", nullable = true)
    private User admin;

    @Column(nullable = false)
    @Type(type = "org.hibernate.type.YesNoType")
    private Boolean initialized = Boolean.FALSE;

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public AirQuality getAirQuality() {
        return airQuality;
    }

    public void setAirQuality(AirQuality airQuality) {
        this.airQuality = airQuality;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    public Boolean getInitialized() {
        return initialized;
    }

    public void setInitialized(Boolean initialized) {
        this.initialized = initialized;
    }

    public Integer getPpm() {
        return ppm;
    }

    public void setPpm(Integer ppm) {
        this.ppm = ppm;
    }
}
