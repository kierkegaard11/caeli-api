package com.maja.caeli.model.state;

public enum AirQuality {

    INIT,
    EXCELLENT,
    GOOD,
    UNHEALTHY,
    HAZARDOUS,
    ERROR;

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
