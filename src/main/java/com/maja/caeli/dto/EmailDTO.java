package com.maja.caeli.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class EmailDTO extends BaseDTO {

    @NotNull
    @Email
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
