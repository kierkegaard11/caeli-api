package com.maja.caeli.dto;

import javax.validation.constraints.Size;

public class ResetPasswordDTO extends BaseDTO {

    @Size(min = 8, max = 50)
    private String newPassword;
    @Size(min = 8, max = 50)
    private String matchPassword;
    private String token;
    private Long userId;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getMatchPassword() {
        return matchPassword;
    }

    public void setMatchPassword(String matchPassword) {
        this.matchPassword = matchPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
