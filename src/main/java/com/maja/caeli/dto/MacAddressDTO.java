package com.maja.caeli.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class MacAddressDTO extends BaseDTO{

    private static final String INVALID_MAC_ADDRESS_FORMAT = "Invalid mac address format!";

    @NotNull
    @Pattern(regexp = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$", message = INVALID_MAC_ADDRESS_FORMAT)
    private String macAddress;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
