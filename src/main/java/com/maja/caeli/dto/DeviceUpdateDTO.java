package com.maja.caeli.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DeviceUpdateDTO extends BaseDTO{

    @NotNull
    @Size(min = 2, max = 15)
    private String name;

    @Size(max = 30)
    private String location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
