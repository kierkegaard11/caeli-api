package com.maja.caeli.api.validator;

import com.maja.caeli.model.User;
import com.maja.caeli.util.CustomSessionUtil;

public abstract class GenericValidator {

    public abstract <T> GenericValidator validate (T id);

    public User getUser() {
        return CustomSessionUtil.getUserFromSession();
    }

}
