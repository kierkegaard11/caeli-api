package com.maja.caeli.api.validator;

import com.maja.caeli.util.LicenceUtil;
import org.springframework.stereotype.Component;

@Component
public class ShareCodeValidator {

    protected static final String SHARE_TOKEN_EXPIRED = "Share code has expired!";
    protected static final String INVALID_SHARE_TOKEN = "Share code is invalid!";

    public boolean validate(String shareCode) {
        String[] shareCodeParts = shareCode.split(LicenceUtil.SHARE_CODE_DELIMITER);
        if (shareCodeParts.length != 2) {
            throw new RuntimeException(INVALID_SHARE_TOKEN);
        } else if (shareCodeParts[0].charAt(0) != LicenceUtil.SHARE_CODE_PREFIX.charAt(0)) {
            throw new RuntimeException(INVALID_SHARE_TOKEN);
        }
        long createdMillis = Long.valueOf(shareCodeParts[0].substring(LicenceUtil.SHARE_CODE_PREFIX.length()), 36);
        long currentMillis = System.currentTimeMillis();
        if (currentMillis - createdMillis > 86400000) {
            throw new RuntimeException(SHARE_TOKEN_EXPIRED);
        } else if (currentMillis - createdMillis < 0) {
            throw new RuntimeException(INVALID_SHARE_TOKEN);
        }
        return true;
    }
}
