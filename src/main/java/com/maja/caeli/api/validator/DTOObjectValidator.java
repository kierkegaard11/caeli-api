package com.maja.caeli.api.validator;

import com.maja.caeli.util.CustomResponseEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;

@Component
public class DTOObjectValidator {

    public ResponseEntity<?> mapValidationService(BindingResult result) {

        if (result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for (FieldError error : result.getFieldErrors()) {
                errorMap.put(error.getField(), error.getDefaultMessage());
            }
            return CustomResponseEntity.error(errorMap);
        }
        return null;
    }
}
