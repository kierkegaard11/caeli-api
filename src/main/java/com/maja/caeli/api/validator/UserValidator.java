package com.maja.caeli.api.validator;

import com.maja.caeli.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserValidator extends GenericValidator{

    protected static final String USER_NOT_ACCESSING_HIS_RESOURCES_ERROR =
            "User tried to access someone else's resources";

    @Override
    public <T> GenericValidator validate(T id) {
        long userId = (long) id;
        User user = getUser();
        if (user.getId() != userId) {
            throw new RuntimeException(USER_NOT_ACCESSING_HIS_RESOURCES_ERROR);
        }
        return this;
    }
}
