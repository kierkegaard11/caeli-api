package com.maja.caeli.api.validator;

import com.maja.caeli.facade.DeviceFacade;
import com.maja.caeli.model.User;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class DeviceValidator extends GenericValidator {

    protected static final String USER_NOT_ACCESSING_HIS_DEVICE = "User tried to access someone else's device";

    @Resource
    private DeviceFacade deviceFacade;

    @Override
    public <T> GenericValidator validate(T id) {
        long deviceId = (long) id;
        User user = getUser();
        if (!deviceFacade.deviceBelongsToUser(user.getId(), deviceId)) {
            throw new RuntimeException(USER_NOT_ACCESSING_HIS_DEVICE);
        }
        return this;
    }
}
