package com.maja.caeli.api.validator;

import com.maja.caeli.exception.UserNotLoggedInException;
import com.maja.caeli.model.User;
import org.springframework.stereotype.Component;

@Component
public class UUIDValidator extends GenericValidator {

    private static final String USER_NOT_LOGGED_IN_ERROR = "User is not logged in on this device";

    @Override
    public <T> GenericValidator validate(T id) {
        String uuid = (String) id;
        User user = getUser();
        if (user.getToken() == null || !user.getToken().equals(uuid)) {
            throw new UserNotLoggedInException(USER_NOT_LOGGED_IN_ERROR);
        }
        return this;
    }
}
