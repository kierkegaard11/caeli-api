package com.maja.caeli.api;

import com.maja.caeli.api.validator.*;
import com.maja.caeli.dto.*;
import com.maja.caeli.exception.CaeliException;
import com.maja.caeli.exception.PasswordResetInvalidTokenException;
import com.maja.caeli.facade.DeviceFacade;
import com.maja.caeli.facade.UserFacade;
import com.maja.caeli.model.User;
import com.maja.caeli.util.CustomResponseEntity;
import com.maja.caeli.util.LicenceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
@RequestMapping("/caeli")
public class Controller {

    public static final String RESET_PASSWORD_PAGE_URL = "/users/reset-password-page";
    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
    private static final String USER_REGISTER_SUCCESS = "User registered successfully!";
    private static final String LOGOUT_SUCCESSFUL = "Logout successful!";
    private static final String DEVICE_DELETE_SUCCESS = "Device deleted successfully!";
    private static final String EMAIL_SENT = "Reset password email sent successfully!";
    private static final String RESET_PASSWORD_PAGE_FILE_LOCATION = "/static/resetPasswordPage.html";
    private static final String RESET_PASSWORD_SUCCESS = "Password reset success";
    private static final String USER_DELETE_SUCCESS = "User deleted successfully!";
    private static final String RESET_PASSWORD_ERROR = "Password reset error";
    private static final String RESET_PASSWORD_SUCCESS_PAGE_FILE_LOCATION = "/static/resetPasswordSuccessPage.html";
    private static final String ERROR_PAGE_FILE_LOCATION = "/static/tokenErrorPage.html";

    @Resource
    private UserFacade userFacade;
    @Resource
    private DTOObjectValidator dtoObjectValidator;
    @Resource
    private DeviceFacade deviceFacade;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private DeviceValidator deviceValidator;
    @Autowired
    private ShareCodeValidator shareCodeValidator;
    @Autowired
    private UUIDValidator uuidValidator;

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUserById(@PathVariable long id, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            userValidator.validate(id);
            UserDTO userDTO = userFacade.getUserById(id);
            return CustomResponseEntity.success(userDTO);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException ex) {
            return CustomResponseEntity.error(ex);
        }
    }

    @PostMapping("/users/login")
    public ResponseEntity<?> login(@RequestBody LoginDTO loginDTO) {
        try {
            JwtResponseDTO jwtResponseDTO = userFacade.authenticate(loginDTO);
            return CustomResponseEntity.success(jwtResponseDTO);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException ex) {
            return CustomResponseEntity.error(ex);
        }
    }

    @PostMapping("/users/register")
    public ResponseEntity<?> register(@RequestBody @Validated UserInfoDTO userInfoDTO, BindingResult result) {
        try {
            ResponseEntity<?> errorMap = dtoObjectValidator.mapValidationService(result);
            if (errorMap != null) {
                return errorMap;
            }
            userFacade.saveUser(userInfoDTO);
            return CustomResponseEntity.success(USER_REGISTER_SUCCESS);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException ex) {
            return CustomResponseEntity.error(ex);
        }
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable long userId, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            userValidator.validate(userId);
            userFacade.deleteUserById(userId);
            return CustomResponseEntity.success(USER_DELETE_SUCCESS);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException ex) {
            return CustomResponseEntity.error(ex);
        }
    }

    @PutMapping("/users/token")
    public ResponseEntity<?> setUserToken(@RequestBody TokenDTO tokenDTO) {
        try {
            userValidator.validate(tokenDTO.getUserId());
            userFacade.updateUserToken(tokenDTO.getUserId(), tokenDTO.getToken());
            return CustomResponseEntity.success(tokenDTO);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException ex) {
            return CustomResponseEntity.error(ex);
        }
    }

    @PutMapping("/users/{userId}/logout")
    public ResponseEntity<?> logout(@PathVariable long userId, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            userValidator.validate(userId);
            userFacade.logout(userId);
            return CustomResponseEntity.success(LOGOUT_SUCCESSFUL);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException ex) {
            return CustomResponseEntity.error(ex);
        }
    }

    @PostMapping(value = "/users/reset-password-via-email")
    ResponseEntity<?> sendResetPasswordEmail(@RequestBody EmailDTO emailDTO) {
        try {
            userFacade.sendResetPasswordEmail(emailDTO.getEmail());
            return CustomResponseEntity.ok(EMAIL_SENT);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException ex) {
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping(value = RESET_PASSWORD_PAGE_URL)
    ResponseEntity<?> resetPasswordPage(@RequestParam int userId, @RequestParam String token) {
        try {
            userFacade.validatePasswordResetToken(userId, token);
            String response = getFileOnThePathAndReturnString(RESET_PASSWORD_PAGE_FILE_LOCATION);
            return ResponseEntity.ok()
                    .contentLength(response.length())
                    .contentType(MediaType.TEXT_HTML)
                    .body(response);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex, HttpStatus.NOT_FOUND);
        } catch (PasswordResetInvalidTokenException e) {
            LOGGER.error(e.getMessage(), e);
            String response = returnTokenErrorHtmlPage(e.getMessage());
            return ResponseEntity.ok()
                    .contentLength(response.length())
                    .contentType(MediaType.TEXT_HTML)
                    .body(response);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage());
        }
    }

    @PostMapping(value = "/users/reset-password")
    ResponseEntity<?> resetPassword(@RequestBody @Validated ResetPasswordDTO resetPasswordDTO, BindingResult result) {
        try {
            ResponseEntity<?> errorMap = dtoObjectValidator.mapValidationService(result);
            if (errorMap != null) {
                return CustomResponseEntity.error("Password must be between 8 and 50 characters in length.");
            }
            userFacade.resetPassword(resetPasswordDTO.getToken(), resetPasswordDTO.getUserId(), resetPasswordDTO.getNewPassword(), resetPasswordDTO.getMatchPassword());
            return CustomResponseEntity.success(RESET_PASSWORD_SUCCESS);
        } catch (RuntimeException e) {
            LOGGER.error(RESET_PASSWORD_ERROR, e);
            return CustomResponseEntity.error(RESET_PASSWORD_ERROR, e.getMessage());
        }
    }

    @GetMapping(value = "/users/reset-password-success")
    ResponseEntity<?> getResetPasswordSuccessPage() {
        try {
            String response = getFileOnThePathAndReturnString(RESET_PASSWORD_SUCCESS_PAGE_FILE_LOCATION);
            return ResponseEntity.ok()
                    .contentLength(response.length())
                    .contentType(MediaType.TEXT_HTML)
                    .body(response);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex, HttpStatus.NOT_FOUND);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @PostMapping(value = "/devices")
    ResponseEntity<?> addNewDevice(@RequestBody @Validated DeviceDTO deviceDTO, @RequestParam String uuid, BindingResult result) {
        try {
            ResponseEntity<?> errorMap = dtoObjectValidator.mapValidationService(result);
            if (errorMap != null) {
                return errorMap;
            }
            uuidValidator.validate(uuid);
            userValidator.validate(deviceDTO.getAdminId());
            DeviceDTO savedDevice = deviceFacade.createNewDevice(deviceDTO);
            return CustomResponseEntity.success(savedDevice);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage(), e.getMessage());
        }
    }

    @PutMapping(value = "/devices/{deviceId}")
    ResponseEntity<?> updateDevice(@PathVariable long deviceId, @RequestBody @Validated DeviceUpdateDTO deviceDTO, @RequestParam String uuid, BindingResult result) {
        try {
            ResponseEntity<?> errorMap = dtoObjectValidator.mapValidationService(result);
            if (errorMap != null) {
                return errorMap;
            }
            uuidValidator.validate(uuid);
            User user = deviceValidator.validate(deviceId).getUser();
            DeviceDTO updatedDevice = deviceFacade.updateDevice(deviceId, deviceDTO, user);
            return CustomResponseEntity.success(updatedDevice);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage(), e.getMessage());
        }
    }

    @DeleteMapping(value = "/devices/{deviceId}")
    ResponseEntity<?> deleteDevice(@PathVariable long deviceId, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            User user = deviceValidator.validate(deviceId).getUser();
            deviceFacade.deleteDevice(deviceId, user);
            return CustomResponseEntity.success(DEVICE_DELETE_SUCCESS);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage(), e.getMessage());
        }
    }

    @GetMapping(value = "/devices/{deviceId}")
    ResponseEntity<?> getDeviceById(@PathVariable long deviceId, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            deviceValidator.validate(deviceId);
            DeviceDTO device = deviceFacade.getDeviceById(deviceId);
            return CustomResponseEntity.success(device);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage(), e.getMessage());
        }
    }

    @GetMapping(value = "/devices")
    ResponseEntity<?> getDevicesByUserId(@RequestParam long userId, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            userValidator.validate(userId);
            List<DeviceDTO> devices = deviceFacade.getDevicesByUserId(userId);
            return CustomResponseEntity.success(devices);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage(), e.getMessage());
        }
    }

    @PostMapping(value = "admin/devices")
    ResponseEntity<?> adminCreateNewDevice(@RequestBody MacAddressDTO macAddressDTO) {
        try {
            DeviceDTO savedDevice = deviceFacade.createDefaultDevice(macAddressDTO.getMacAddress());
            return CustomResponseEntity.success(savedDevice);
        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage(), e.getMessage());
        }
    }

    @GetMapping("/devices/share-device-code")
    public ResponseEntity<?> getShareDeviceCode(@RequestParam long deviceId, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            User user = deviceValidator.validate(deviceId).getUser();
            DeviceShareCodeDTO deviceShareCodeDTO = deviceFacade.getShareCodeByDeviceId(deviceId, user);
            return CustomResponseEntity.success(deviceShareCodeDTO);
        } catch (CaeliException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping("/devices/{deviceId}/members")
    public ResponseEntity<?> getDeviceMembers(@PathVariable long deviceId, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            deviceValidator.validate(deviceId).getUser();
            List<UserDTO> members = deviceFacade.getDeviceMembers(deviceId);
            return CustomResponseEntity.success(members);
        } catch (CaeliException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @PostMapping("/devices/get-shared-device")
    public ResponseEntity<?> getSharedDevice(@RequestBody TokenDTO tokenDTO, @RequestParam String uuid) {
        try {
            uuidValidator.validate(uuid);
            User user = userValidator.validate(tokenDTO.getUserId()).getUser();
            String shareCode = tokenDTO.getToken();
            shareCodeValidator.validate(shareCode);
            String licenceCode = LicenceUtil.getLicenceCodeFromShareCode(shareCode);
            DeviceDTO deviceDTO = deviceFacade.shareDeviceWithUser(user.getId(), licenceCode);
            return CustomResponseEntity.success(deviceDTO);
        } catch (CaeliException e) {
            LOGGER.warn(e.getMessage(), e);
            return CustomResponseEntity.error(e);
        } catch (RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        }
    }

    @GetMapping(value = "/devices/states")
    ResponseEntity<?> updateDeviceState(@RequestParam String l, @RequestParam int a, @RequestParam int h, @RequestParam int t) {
        try {
            deviceFacade.updateDeviceStates(l, a, h, t);
            LOGGER.info("State updated l = " + l + " air = " + a + " humidity = " + h + " temp = " + t);
            return CustomResponseEntity.success("OK");

        } catch (CaeliException ex) {
            LOGGER.warn(ex.getMessage(), ex);
            return CustomResponseEntity.error(ex);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage(), e);
            return CustomResponseEntity.error(e.getMessage(), e.getMessage());
        }
    }

    private String getFileOnThePathAndReturnString(String path) throws IOException {
        InputStream pageStream = getClass().getResourceAsStream(path);
        if (pageStream == null || pageStream.available() <= 0) {
            throw new FileNotFoundException("Page file not found!!!");
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(pageStream));
        String response = reader.lines()
                .collect(Collectors.joining("\n"));
        return response;
    }

    private String returnTokenErrorHtmlPage(String message) {
        try {
            String response = getFileOnThePathAndReturnString(ERROR_PAGE_FILE_LOCATION);
            response = response.replaceAll("\\$\\{message}", message);
            return response;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error";
        }
    }
}
