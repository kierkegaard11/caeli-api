package com.maja.caeli.dao;

import com.maja.caeli.model.UserDeviceAggr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDeviceAggrDAO extends JpaRepository<UserDeviceAggr, Long>, JpaSpecificationExecutor<UserDeviceAggr> {

    @Query("SELECT uda FROM UserDeviceAggr uda WHERE uda.user.id=?1")
    List<UserDeviceAggr> findByUserId(long userId);

    @Query("SELECT uda FROM UserDeviceAggr uda WHERE uda.user.id=?1 AND uda.device.id=?2")
    UserDeviceAggr findByUserIdAndDeviceId(Long userId, long deviceId);

    @Query("SELECT uda FROM UserDeviceAggr uda WHERE uda.device.id=?1")
    List<UserDeviceAggr> findByDeviceId(long deviceId);
}
