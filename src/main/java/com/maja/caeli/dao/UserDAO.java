package com.maja.caeli.dao;

import com.maja.caeli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface UserDAO extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    Optional<User> findByEmail(@Param("email") String email);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.token = ?2 WHERE u.id = ?1")
    void updateUserToken(Long userId, String token);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET u.token = null WHERE u.id = ?1")
    void deleteToken(long userId);
}
