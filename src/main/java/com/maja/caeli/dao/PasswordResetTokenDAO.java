package com.maja.caeli.dao;

import com.maja.caeli.model.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Repository
public interface PasswordResetTokenDAO extends JpaRepository<PasswordResetToken, Long>, JpaSpecificationExecutor<PasswordResetToken> {

    @Query("SELECT prt FROM PasswordResetToken prt WHERE prt.token = ?1")
    Optional<PasswordResetToken> findByToken(String token);

    @Query("SELECT prt FROM PasswordResetToken prt WHERE prt.user.id = ?1")
    Optional<PasswordResetToken> findByUserId(Long userID);

    @Transactional
    @Modifying
    @Query("UPDATE PasswordResetToken prt SET prt.token = ?2, prt.expiryDate = ?3 WHERE prt.id = ?1")
    void update(Long passwordResetTokenID, String token, Date expiryDate);

    @Transactional
    @Modifying
    @Query("DELETE FROM PasswordResetToken prt WHERE prt.user.id = ?1")
    void deleteByUserId(Long userID);
}
