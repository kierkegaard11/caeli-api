package com.maja.caeli.dao;

import com.maja.caeli.model.Device;
import com.maja.caeli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeviceDAO extends JpaRepository<Device, Long>, JpaSpecificationExecutor<Device> {

    @Query("SELECT d FROM Device d WHERE d.licence=?1")
    Optional<Device> findByLicence(String licence);

    @Query("SELECT d FROM Device d WHERE d.admin.id=?1")
    List<Device> findByAdminId(long adminId);
}
